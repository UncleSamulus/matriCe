#include <stdio.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

const char* RED = "\x1B[31m";
const char* GRN = "\x1B[32m";
const char* YEL = "\x1B[33m";
const char *BLU ="\x1B[34m";
const char *MAG = "\x1B[35m";
const char *CYN = "\x1B[36m";
const char *WHT = "\x1B[37m";
const char *RESET = "\x1B[0m";

typedef struct {
    char *set;
    unsigned int length;
} CHARSET;

unsigned int str_to_idx(char* color)
{
    const char *colors[] = {"red", "green", "yellow", "blue", "magenta", "cyan", "white"};
    for (int idx = 0; idx < 7; idx++)
    {
        if (color == colors[idx])
            return idx;
    }
    return -1;
}

const char *str_to_ansi(char *color)
{
    unsigned int idx;
    const char *ansis[] = {RED, GRN, YEL, BLU, MAG, CYN, WHT};
    idx = str_to_idx(color);
    if (idx == -1) 
        return RESET;
    else 
        return ansis[idx];
}

void usage()
{
    printf("\n");
    printf("matriCe usage help :\n");
    for (int i = 0; i < 20; i++)
    {
        printf("=");
    }
    printf("\n\n");
    printf(" -h -- show usage help\n");
    printf(" -b -- use only binary number\n");
    printf(" -c COLOR -- set main color\n");
    printf(" -s SPEED -- set speed in millisecond\n");
    printf(" -i -- switch to interactive mode\n");
    printf("\n"); 
}

void delay(int millis)
{
    // Storing start time
    clock_t start_time = clock();
  
    // looping till required time is not achieved
    while (clock() < start_time + millis)
        ;
}

void matrix(char *color, int speed, CHARSET charset)
{
    const char *color_code = str_to_ansi(color);
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    char *previous = (char *) malloc((w.ws_col+1) * sizeof(char));
    char *current = (char *) malloc((w.ws_col+1) * sizeof(char));
    char letter;
    for (int i = 0; i < w.ws_col; i++)
    {
        previous[i] = ' ';
    }
    bool end = false;
    while (! end)
    {
        delay(speed);
        for (int i = 0; i < w.ws_col; i++)
        {
            unsigned int rd = rand() % 100;
            if (rd < 50) {
                rd = rand() % charset.length;
                letter = charset.set[rd];
            } else {
                letter = ' ';
            }
            current[i] = letter;
        }
        printf("%s%s%s\n", color_code, current, RESET);
        memcpy(previous, current, sizeof(current));
    }
}

int main(int argc, char **argv)
{
    bool binary = true;
    bool interactive = false;
    int i;
    char *color = "green";
    int speed = 1000; // millis
    CHARSET charset;
    srand(time(NULL));
    while (1)
    {
        char c;
        c = getopt(argc, argv, "hbc:s:i");
        if (c == -1)
        {
            break;
        }
        switch (c)
        {
            case 'h':
                usage();
                exit(0);
            case 'b':
                binary = true;
                break;
            case 'c':
                color = optarg;
                break;
            case 's':
                speed = atoi(optarg);
                break;
            case 'i':
                interactive = true;
                break;
            case '?':
            default:
                usage();
                exit(0);
        }
    }
    if (binary)
    {
        charset.length = 2;
        charset.set = "01";
    }
    matrix(color, speed, charset);
    return 0;
}